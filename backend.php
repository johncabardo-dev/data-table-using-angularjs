<?php 
header("Access-Control-Allow-Origin: *");
require 'database.php';
require 'model.php';
$db    = new Database('127.0.0.1','root','','sampledb');
$model = new Model($db);

if(@$_GET['func'] === 'getUserData')
{
	$model->getUserData();
}
elseif(@$_GET['func'] === 'getProduct')
{
	$model->getProduct($_POST["id"]);
}
elseif(@$_GET['func'] === 'addUser')
{
	$fname = $_POST['fname'];
	$lname = $_POST['lname'];
	$state = $_POST['state'];
	$phone = $_POST['phone'];
	$email = $_POST['email'];
	$password = $_POST['password'];

	$data = array(
		'last_name' 		=> $lname,
		'first_name'		=> $fname,
		'state'				=> $state,
		'phone'				=> $phone,
		'email'				=> $email,
		'password'			=> $password
		);

	$model->addUser($data);
}
elseif(@$_GET['func'] === 'editUser')
{
	$model->editUser($_POST["userid"]);
}
elseif(@$_GET['func'] === 'updateUser')
{
	$fname = $_POST['fname'];
	$lname = $_POST['lname'];
	$state = $_POST['state'];
	$phone = $_POST['phone'];
	$email = $_POST['email'];
	$userid = $_POST['userid'];

	$data = array(
		'last_name' 		=> $lname,
		'first_name'		=> $fname,
		'state'				=> $state,
		'phone'				=> $phone,
		'email'				=> $email,
		'userid'			=> $userid
		);

	$model->updateUser($data);
}
elseif(@$_GET['func'] === 'deleteUser')
{
	$model->deleteUser($_POST["userid"]);
}
elseif(@$_GET['func'] === 'searchUser')
{
	$model->searchUser($_POST["lname"]);
}
else
{
	die(json_encode('Restricted Area!'));
}

?>