<?php
class Database
{
    private $host;
    private $user;
    private $password;
    private $data;
    private $mysqli;


    public function __construct($host,$user,$pass,$data) {

        $this->host     = $host;
        $this->user     = $user;
        $this->pass     = $pass;
        $this->data     = $data;
        $this->mysqli   = new mysqli($this->host, $this->user, $this->pass, $this->data);
    }

    public function query($query)
    {
        return $this->mysqli->query($query);
    }
}
?>