<!DOCTYPE html>
<html ng-app="myApp">
	<head>
		<title>AngularJS DataTable </title>
		<link href="images/favicon.ico" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" type="text/css" href="css/semantic.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<script src="js/semantic.js"></script>
		<script src="js/angular.js"></script>
        <script src="js/dirPagination.js"></script>
    	<script src="js/controllers.js"></script>
    	<script src="js/services.js"></script>
    	<style>
		</style>
	</head>

	<body>
		<div ng-controller="main" style="margin-top: 30px;">
			<div class="ui grid pad">
				<div class="eight wide column">
					<div class="ui floating message">
						<span>Country : <img ng-src="{{src}}" width="30"/> {{ country }}{{ currency }}{{ symbol }}{{ code }}</span>
					</div>
				</div>
			</div>
			<div class="ui grid pad">
				<div class="one wide column">
					<button type="button" class="massive ui teal button icon" ng-click="openModal()"><i class="add user icon"></i></button>
				</div>
			</div>
			<div class="ui grid pad">
			</div>
			<div class="ui grid pad">
				<div class="sixteen wide column" ng-if="success" >
					<div class="ui success message">
						<div class="header">
						    {{ alert_msg }}
						</div>
					</div>
				</div>

				<div class="sixteen wide column" ng-if="error" >
					<div class="ui negative message" ng-if="error">
					  	<div class="header">
					    	{{ alert_msg }}
					  	</div>
					</div>
				</div>
			</div>
			<div class="ui modal add_modal">
			  	<div class="header">Registration Form</div>
			  	<div class="content">
			    	<form class="ui form" name="addForm" ng-submit="addUser(addForm)" novalidate>
					  	<div class="field">
					    	<label>Lastname</label>
					    	<input type="text" ng-model="data.lname" name="lname" placeholder="Enter lastname" required>
					  	</div>
					  	<div class="field">
					    	<label>Firstname</label>
					    	<input type="text" ng-model="data.fname" name="fname" placeholder="Enter firstname" required>
					  	</div>
					  	<div class="field">
					    	<label>State</label>
					    	<input type="text" ng-model="data.state" name="state" placeholder="Enter state" required>
					  	</div>
					  	<div class="field">
					    	<label>Phone</label>
					    	<input type="text" ng-model="data.phone" name="phone" placeholder="Enter phone" required>
					  	</div>
					  	<div class="field">
					    	<label>Email</label>
					    	<input type="email" ng-model="data.email" name="email" placeholder="Enter email" required>
					  	</div>
					  	<div class="field">
					    	<label>Password</label>
					    	<input type="text" ng-model="data.password" name="password" placeholder="Enter password" required>
					  	</div>
					  	<button class="ui positive button" type="submit"><i class="add user icon"></i> Submit</button>
					</form>
			  	</div>
			</div>

			<div class="ui grid pad">
				<div class="four column row">
					<div class="left floated column">
						<div class="ui fluid category search">
							<div class="ui icon input fluid">
							    <input class="prompt " type="text" ng-model="data.search" placeholder="Search keyword">
							    <i class="search icon"></i>
							</div>
						</div>
					</div>
    				<div class="right floated column">
    					<div class="input-group perItems" ng-show="(datas|filter:data.search).length != 0">
					    	Show:
						    <select ng-model="data.per_items">
						      <option value="{{data.per_items1}}" selected>5</option>
						      <option value="10">10</option>
						      <option value="50">50</option>
						      <option value="100">100</option>
						      <option value="500">500</option>
						    </select>
						    entries
						</div> 
    				</div>
				</div>
			</div>

			<div class="ui grid pad">
				<div class="sixteen wide column">
					<table  class="ui teal table ">
						<thead>
						  	<tr>
							    <th ng-click="orderBy('userid')">UserId <i class="sort icon"></i></th>
							    <th ng-click="orderBy('refid')">RefId <i class="sort icon"></i></th>
							    <th ng-click="orderBy('last_name')">Lastname <i class="sort icon"></i></th>
							    <th ng-click="orderBy('first_name')">Firstname <i class="sort icon"></i></th>
							    <th ng-click="orderBy('state')">State <i class="sort icon"></i></th>
							    <th ng-click="orderBy('phone')">Phone <i class="sort icon"></i></th>
							    <th ng-click="orderBy('password')">Email <i class="sort icon"></i></th>
							    <th ng-click="orderBy('password')">Password <i class="sort icon"></i></th>
							    <th ng-click="orderBy('type')">Type <i class="sort icon"></i></th>
							    <th ng-click="orderBy('subscribe')">Subscribe <i class="sort icon"></i></th>
							    <th ng-click="orderBy('date_register')">Date Register <i class="sort icon"></i></th>
							    <th>Action</th>
						  	</tr>
						</thead>
						<tbody>
						  	<tr dir-paginate="info in filtered = (datas | filter:data.search | orderBy:myOrderBy:reverse | itemsPerPage:data.per_items)  track by $index " ng-if="data_length > 0">
							    <td>{{ info.userid }}</td>
							    <td>{{ info.refid }}</td>
							    <td>{{ info.last_name }}</td>
							    <td>{{ info.first_name }}</td>
							    <td>{{ info.state }}</td>
							    <td>{{ info.phone }}</td>
							    <td>{{ info.email }}</td>
							    <td>{{ info.password }}</td>
							    <td>{{ info.type }}</td>
							    <td>{{ info.subscribe }}</td>
							    <td>{{ info.date_register }}</td>
							    <td>
							    	<div class="ui buttons">
									  	<button id="editModal" ng-click="edit(info.userid)" class="medium blue ui icon button"><i class="write icon"></i>&nbsp;&nbsp;</button>
									  	<div class="or"></div>
									  	<button class="medium red ui icon button" ng-click="openRemoveModal(info.userid)">&nbsp;&nbsp;&nbsp;<i class="trash icon" ></i></button>
									</div>
							    </td>
						  	</tr>
						</tbody>
					</table>

					<center>
						<dir-pagination-controls
				        	max-size="4"
				        	direction-links="true"
				        	boundary-links="true" >
				    	</dir-pagination-controls> 
				    </center>

				  	<div class="ui grid pad" ng-show="(datas|filter:data.search).length == 0" style="text-align: center;">
						<div class="sixteen wide column">
							<div class="ui negative message">
								<div class="header">
								    No data found !
								</div>
							</div>
						</div>
					</div>

					<div class="ui modal edit_modal">
					  	<div class="header">Edit Form</div>
					  	<div class="content">
							<form class="ui form edit" name="editForm" ng-submit="update_user(editForm,data.id)" novalidate>
							  	<div class="field">
							    	<label>Lastname</label>
							    	<input type="text" ng-model="data.lname" name="lname" placeholder="Enter lastname" required>
							  	</div>
							  	<div class="field">
							    	<label>Firstname</label>
							    	<input type="text" ng-model="data.fname" name="fname" placeholder="Enter firstname" required >
							  	</div>
							  	<div class="field">
							    	<label>State</label>
							    	<input type="text" ng-model="data.state" name="state" placeholder="Enter state" required >
							  	</div>
							  	<div class="field">
							    	<label>Phone</label>
							    	<input type="text" ng-model="data.phone" name="phone" placeholder="Enter phone" required >
							  	</div>
							  	<div class="field">
							    	<label>Email</label>
							    	<input type="email" ng-model="data.email" name="email" placeholder="Enter email" required >
							  	</div>
							  	<button class="ui positive button" type="submit"><i class="write icon"></i> Update</button>
							</form>
					  	</div>
					</div>

					<div class="ui small modal remove_modal">
					  	<div class="header">Are you sure you want to delete this user ?</div>
					  	<div class="content">
					  		<div class="ui grid">
					  			<div class="eight wide column">
								  	<button class="ui labeled icon button green fluid" ng-click="removeUser(user_id)">
									  	<i class="checkmark icon"></i>
									  	Yes
									</button>
								</div>
					  			<div class="eight wide column">
									<button class="ui right labeled icon button red fluid" ng-click="closeRemoveModal()">
									  	<i class="remove icon"></i>
									  	No
									</button>
								</div>
							</div>
					  	</div>
					</div>
					
				</div>
			</div>
		</div>

		<script type="text/javascript">
			$('.ui.form')
			  	.form({
			    fields: {
			      lname: {
			        identifier: 'lname',
			        rules: [
			          {
			            type   : 'empty',
			            prompt : 'Required'
			          }
			        ]
			      },
			      fname: {
			        identifier: 'fname',
			        rules: [
			          {
			            type   : 'empty',
			            prompt : 'Required'
			          }
			        ]
			      },
			      state: {
			        identifier: 'state',
			        rules: [
			          {
			            type   : 'empty',
			            prompt : 'Required'
			          }
			        ]
			      },
			      phone: {
			        identifier: 'phone',
			        rules: [
			          {
			            type   : 'number',
			            prompt : 'Please enter valid number'
			          },
			          {
			            type   : 'empty',
			            prompt : 'Required'
			          }
			        ]
			      },
			      password: {
			        identifier: 'password',
			        rules: [
			          {
			            type   : 'empty',
			            prompt : 'Required'
			          }
			        ]
			      },
			      email: {
			        identifier: 'email',
			        rules: [
			          {
			            type   : 'email',
			            prompt : 'Please enter valid email'
			          },
			          {
			            type   : 'empty',
			            prompt : 'Required'
			          }
			        ]
			      }
			    }
			  })
			;

			$('.ui.form.edit')
			  	.form({
			    fields: {
			      lname: {
			        identifier: 'lname',
			        rules: [
			          {
			            type   : 'empty',
			            prompt : 'Required'
			          }
			        ]
			      },
			      fname: {
			        identifier: 'fname',
			        rules: [
			          {
			            type   : 'empty',
			            prompt : 'Required'
			          }
			        ]
			      },
			      state: {
			        identifier: 'state',
			        rules: [
			          {
			            type   : 'empty',
			            prompt : 'Required'
			          }
			        ]
			      },
			      phone: {
			        identifier: 'phone',
			        rules: [
			          {
			            type   : 'number',
			            prompt : 'Please enter valid number'
			          },
			          {
			            type   : 'empty',
			            prompt : 'Required'
			          }
			        ]
			      }
			    }
			  })
			;
		</script>
	</body>
</html>