var app = angular.module('myApp', ['myApp.services', 'angularUtils.directives.dirPagination']);


app.controller('main', function(Auth, $scope, $http, $rootScope, $timeout) {
  $scope.data = [];
  $scope.success = false;
  $scope.error = false;
  $scope.data.search = "";
  $scope.data.per_items = 5;
  $scope.data.per_items1 = 5;
  $scope.reverse = false;

	$scope.initialize = function()
	{
    $timeout(function(){
      $scope.success = false;
      $scope.error = false;
    },10000);

    var obj    = new Object();
    obj.method = 'POST';
    obj.url    = "http://ip-api.com/json";
    obj.data   = new FormData();
    obj.params = {};
    Auth.REQUEST(obj).then(function(success) {
                              $scope.country = success.data.country;
                              $scope.countryCode = success.data.countryCode;
                              var obj    = new Object();
                              obj.method = 'POST';
                              obj.url    = "currency.json";
                              obj.data   = new FormData();
                              obj.params = {};
                              Auth.REQUEST(obj).then(function(success) {
                                                        $.each(success.data, function( index, value ) {
                                                          if(index ==  $scope.countryCode)
                                                          {
                                                            $scope.currencyCode = value;
                                                            var obj    = new Object();
                                                            obj.method = 'POST';
                                                            obj.url    = "currencySymbol.json";
                                                            obj.data   = new FormData();
                                                            obj.params = {};
                                                            Auth.REQUEST(obj).then(function(success) {
                                                                                      $.each(success.data, function( index, value ) {
                                                                                        if(index ==  $scope.currencyCode)
                                                                                        {
                                                                                          $scope.currency = " | Currrency Name : "+value.name;
                                                                                          $scope.symbol = " | Symbol : "+value.symbol_native;
                                                                                          $scope.code = " | Code : " + value.code;
                                                                                          $scope.src = "http://www.geonames.org/flags/x/" + $scope.countryCode.toLowerCase() + ".gif";
                                                                                        }
                                                                                      });
                                                                                  },
                                                                                  function(error) { 
                                                                                      $scope.initialize();
                                                                                    }
                                                                                  );
                                                          }
                                                        });
                                                    },
                                                    function(error) { 
                                                        $scope.initialize();
                                                      }
                                                    );
                          },
                          function(error) { 
                              $scope.initialize();
                            }
                          );
    

		var obj    = new Object();
    obj.method = 'POST';
    obj.url    = $rootScope.baseURL + "?func=getUserData";
    obj.data   = new FormData();
    obj.params = {};
    Auth.REQUEST(obj).then(function(success) {
    						              $scope.datas = success.data;
                              $scope.data_length = success.data.length; 
                          },
                          function(error) { 
                              $scope.initialize();
                            }
                          );

	}	
  $scope.openModal = function()
  {
    $scope.data.fname = '';
    $scope.data.lname = '';
    $scope.data.state = '';
    $scope.data.phone = '';
    $scope.data.email = '';
    $scope.data.password = '';
    $('.ui.modal.add_modal')
      .modal({
      blurring: true
    })
    .modal('show');
  }
  $scope.openRemoveModal = function(id)
  {
    $scope.user_id = id;
    $('.ui.modal.remove_modal')
      .modal({
      blurring: true
    })
    .modal('show');
  }
  $scope.closeRemoveModal = function(id)
  {
    $('.ui.modal.remove_modal')
    .modal('hide');
  }
	$scope.edit = function(id)
	{
    $('.ui.modal.edit_modal')
      .modal({
      blurring: true
    })
    .modal('show');
		var obj    = new Object();
    obj.method = 'POST';
    obj.url    = $rootScope.baseURL + "?func=editUser";
    obj.data   = new FormData();
    obj.data.append('userid',id);
    obj.params = {};
    Auth.REQUEST(obj).then(function(success) {
    						            $scope.data.lname = success.data[0].last_name;
                            $scope.data.fname = success.data[0].first_name;
                            $scope.data.state = success.data[0].state;
                            $scope.data.phone = success.data[0].phone;
                            $scope.data.email = success.data[0].email;
                            $scope.data.id = success.data[0].userid;
                          },
                          function(error) { 
                              $scope.initialize();
                            }
                          ); 
	}

  $scope.addUser = function(data)
  {
    if(data.$valid && !isNaN($scope.data.phone))
    {
      $('.ui.modal.add_modal').modal('hide');
      var obj    = new Object();
      obj.method = 'POST';
      obj.url    = $rootScope.baseURL + '?func=addUser';
      obj.data   = new FormData();
      obj.data.append('fname',$scope.data.fname);
      obj.data.append('lname',$scope.data.lname);
      obj.data.append('state',$scope.data.state);
      obj.data.append('phone',$scope.data.phone);
      obj.data.append('email',$scope.data.email);
      obj.data.append('password',$scope.data.password);
      obj.params = {};
      Auth.REQUEST(obj).then(function(success) {
                                console.log(success.data);
                                if(success.data == '1')
                                {
                                  $scope.data.fname = '';
                                  $scope.data.lname = '';
                                  $scope.data.state = '';
                                  $scope.data.phone = '';
                                  $scope.data.email = '';
                                  $scope.data.password = '';
                                  $('#addModal').modal('hide');
                                  $scope.alert_msg = "Successfully Added !";
                                  $scope.success = true;
                                }
                                else
                                {
                                  $scope.alert_msg = "Failed to add user !";
                                  $scope.error = true;
                                }
                                $scope.initialize();
                            },
                            function(error) { 
                                console.log(error);
                                $scope.initialize();
                              }
                            );
    }
  }

  $scope.removeUser = function(id)
  {
    $('.ui.modal.remove_modal').modal('hide');
    var obj    = new Object();
    obj.method = 'POST';
    obj.url    = $rootScope.baseURL + '?func=deleteUser';
    obj.data   = new FormData();
    obj.data.append('userid',id);
    obj.params = {};
    Auth.REQUEST(obj).then(function(success) {
                              if(success.data == '1')
                              {
                                $scope.alert_msg = "Successfully Deleted !";
                                $scope.success = true;
                              }
                              else
                              {
                                $scope.alert_msg = "Failed to delete user !";
                                $scope.error = true;
                              }
                              $scope.initialize();
                          },
                          function(error) { 
                              console.log(error);
                              $scope.initialize();
                            }
                          );
  }

  $scope.update_user = function(data, id)
  {
    if(data.$valid && !isNaN($scope.data.phone))
    {
      $('.ui.modal.edit_modal').modal('hide');
      var obj    = new Object();
      obj.method = 'POST';
      obj.url    = $rootScope.baseURL + '?func=updateUser';
      obj.data   = new FormData();
      obj.data.append('fname',$scope.data.fname);
      obj.data.append('lname',$scope.data.lname);
      obj.data.append('state',$scope.data.state);
      obj.data.append('phone',$scope.data.phone);
      obj.data.append('email',$scope.data.email);
      obj.data.append('userid',id);
      obj.params = {};
      Auth.REQUEST(obj).then(function(success) {
                                if(success.data == '1')
                                {
                                  $scope.data.fname = '';
                                  $scope.data.lname = '';
                                  $scope.data.state = '';
                                  $scope.data.phone = '';
                                  $scope.data.email = '';
                                  $scope.alert_msg = "Successfully Updated !";
                                  $scope.success = true;
                                }
                                else
                                {
                                  $scope.alert_msg = "Failed to update user !";
                                  $scope.error = true;
                                }
                                $scope.initialize();
                            },
                            function(error) { 
                                console.log(error);
                                $scope.initialize();
                              }
                            );
    }
  }

  $scope.search = function()
  {
    console.log($scope.data.search);
    var obj    = new Object();
    obj.method = 'POST';
    obj.url    = $rootScope.baseURL + "?func=searchUser";
    obj.data   = new FormData();
    obj.data.append('lname',$scope.data.search);
    obj.params = {};
    Auth.REQUEST(obj).then(function(success) {
                              $scope.datas = success.data;
                              $scope.data_length = success.data.length; 
                          },
                          function(error) { 
                              $scope.initialize();
                            }
                          );
  }

  $scope.orderBy = function(x) {
    $scope.myOrderBy = x;
    $scope.reverse = !$scope.reverse;
  }

	$scope.initialize(); //initialization

});

