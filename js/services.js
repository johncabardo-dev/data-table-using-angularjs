var app = angular.module('myApp.services', []);
app.factory('Auth', ['$http','$rootScope', 
			 function($http,  $rootScope)
{

	$rootScope.baseURL  = 'http://localhost/sample_angularjs/backend.php'; //change to base_url of your site

	var fac = {};
	fac.REQUEST = function(obj) //http request
	{ 
		var http = $http(
			                {
			                    method            : obj.method,
			                    url               : obj.url,
			                    data              : obj.data,
			                    params            : obj.params,
			                    transformRequest  : angular.identity,
			                    headers           : { 'Content-Type':undefined }
			                }
	                    );
		return http;
	}	

	return fac;
}]);