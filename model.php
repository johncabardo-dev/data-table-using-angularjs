<?php
class Model
{
    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    public function getUserData(){
		$results = array();
        $res = $this->db->query("SELECT * FROM users ORDER BY userid ASC");
        foreach ($res as $row) {
		    array_push($results,$row);
		}
        die(json_encode($results));
    }

    public function getProduct($id)
    {
    	$results = array();
		$res = $this->db->query('SELECT * FROM products LEFT JOIN users on products.userid=users.userid WHERE users.userid = '.$id);
		foreach ($res as $row) {
		    array_push($results,$row);
		}
		die(json_encode($results));
    }

    public function addUser($data)
    {
    	$lname = ucwords(@$data["last_name"]);
		$fname = ucwords(@$data["first_name"]);
		$state = ucwords(@$data["state"]);
		$phone = @$data["phone"];
		$email =  @$data["email"];
		$password = @$data["password"];
		$t=time();
		$sql = "INSERT INTO users (refid, last_name, first_name, state, phone, email, password, type, subscribe, date_register) VALUES ('test','$lname', '$fname', '$state', '$phone', '$email', '$password',  '2', 'Yes', '$t')";
		$res = $this->db->query($sql);
		if($res)
		{
			// 1 for success
			die(json_encode(1));

		}
		else
		{
			// 0 for error
			die(json_encode(0));

		}
    }

    public function editUser($id)
    {
    	$results = array();
		$res = $this->db->query('SELECT * FROM users  WHERE userid = '.$id);
		foreach ($res as $row) {
		    array_push($results,$row);
		}
		die(json_encode($results));
    }

    public function updateUser($data)
    {
    	$lname = ucwords($data["last_name"]);
		$fname = ucwords($data["first_name"]);
		$state = ucwords($data["state"]);
		$phone = $data["phone"];
		$email =  $data["email"];
		$userid = $data["userid"];
		$t=time();
		$sql = "UPDATE users SET last_name='$lname', first_name='$fname', state='$state', phone='$phone', email='$email'  WHERE userid='$userid' ";
		$res = $this->db->query($sql);
		if($res)
		{
			// 1 for success
			die(json_encode(1));

		}
		else
		{
			// 0 for error
			die(json_encode(0));

		}
    }

    public function deleteUser($id)
    {
    	$results = array();
    	$sql = "DELETE FROM users WHERE userid='$id' ";
		$res = $this->db->query($sql);
		if($res)
		{
			// 1 for success
			die(json_encode(1));

		}
		else
		{
			// 0 for error
			die(json_encode(0));

		}
    }

    public function searchUser($lname)
    {
    	$results = array();
        $res = $this->db->query("SELECT * FROM users WHERE last_name LIKE '%$lname%' ORDER BY userid ASC ");
        foreach ($res as $row) {
		    array_push($results,$row);
		}
        die(json_encode($results));
    }
}
?>