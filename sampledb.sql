-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 27, 2016 at 11:04 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sampledb`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
`id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `product_title` varchar(150) NOT NULL,
  `product_price` decimal(6,2) NOT NULL,
  `product_img` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `userid`, `product_title`, `product_price`, `product_img`) VALUES
(12, 139, 'Greatest Hits', '8.99', 'product_images/002.jpg'),
(13, 141, 'Brilliant Band', '11.99', 'product_images/003.jpg'),
(14, 143, 'Super Duper', '9.99', 'product_images/004.jpg'),
(15, 160, 'Random Band', '8.99', 'product_images/005.jpg'),
(16, 157, 'Guitar Heroes', '7.99', 'product_images/006.jpg'),
(17, 159, 'Some Randomers', '4.99', 'product_images/007.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`userid` mediumint(8) NOT NULL,
  `refid` char(8) NOT NULL,
  `last_name` char(60) NOT NULL,
  `first_name` char(60) NOT NULL,
  `state` char(60) NOT NULL,
  `phone` char(20) NOT NULL,
  `email` char(60) NOT NULL,
  `password` char(60) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `subscribe` char(4) NOT NULL DEFAULT 'Yes',
  `date_register` char(10) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=218 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `refid`, `last_name`, `first_name`, `state`, `phone`, `email`, `password`, `type`, `subscribe`, `date_register`) VALUES
(139, 'cqvvdfmi', 'Appia', 'Lucie', 'Massachusetts', '123', 'afua49@gmail.com', 'yqhckjym', 2, 'Yes', '1338848174'),
(140, 'h02ya2rq', 'Osei-Tutu', 'Kwaku', 'Georgia', '456', 'k_oseitutu@yahoo.com', 'medofo1pe', 2, 'Yes', '1339084475'),
(141, 'u0djggcq', 'Sam', 'Ann', 'Alabama', '4015724752', 'Araba.Kraba@gmail.com', 'tcnqpjyx', 2, 'Yes', '1340133767'),
(142, 'v6dykip9', 'Locke', 'David', 'Massachusetts', '617 627-2419', 'david.locke@tufts.edu', 'wzwjxfwy', 2, 'Yes', '1340292631'),
(143, '7dykcad7', 'Berman', 'Doug', 'Massachusetts', '789', 'doug@cartalk.com', 'yamputz1', 2, 'Yes', '1340292778'),
(157, 'mkgw82w8', 'Charles', 'Kagunye', 'Massachusetts', '101112', 'ckaggs@yahoo.com', 'kjncycxv', 2, 'Yes', '1340372316'),
(158, 'ked7ys3m', 'Collins', 'Chris', 'Washington', '123123', 'chriscollins101@gmail.com', 'fbbsnbzd', 2, 'Yes', '1340391439'),
(159, 'kvoaod9e', 'Boamah', 'Benjamin', 'Massachusetts', '123123', 'BenjaminDBoamah@gmail.com', 'rrjwtqcd', 2, 'Yes', '1340658924'),
(160, 'uurkxjpx', 'Baffoe', 'Jonathan', 'Massachusetts', '7742535994', 'joetopforme@yahoo.com', 'xszmxfkw', 2, 'Yes', '1340926596'),
(161, '3vfzmqql', 'Priscilla', 'Baffoe', 'Massachusetts', '8572105677', 'loviayaa@yahoo.com', 'cvfsszxn', 2, 'Yes', '1340926693');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`id`), ADD KEY `userid` (`userid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `userid` mediumint(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=218;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
